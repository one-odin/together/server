package cfg

import (
	"github.com/savsgio/atreugo/v11"
)

var Config = atreugo.Config{
	Addr: "0.0.0.0:8000",
}
