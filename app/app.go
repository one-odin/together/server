package main

import (
	"github.com/savsgio/atreugo/v11"
	"gitlab.com/odin-together/server/cfg"
)

func main() {
	server := atreugo.New(cfg.Config)

	server.GET("/", func(ctx *atreugo.RequestCtx) error {
		return ctx.TextResponse("Hello World")
	})

	server.GET("/echo/{path:*}", func(ctx *atreugo.RequestCtx) error {
		return ctx.TextResponse("Echo message: " + ctx.UserValue("path").(string))
	})

	v1 := server.NewGroupPath("/v1")
	v1.GET("/", func(ctx *atreugo.RequestCtx) error {
		return ctx.TextResponse("Hello V1 Group")
	})

	if err := server.ListenAndServe(); err != nil {
		panic(err)
	}
}
